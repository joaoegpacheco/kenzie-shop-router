import React from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css'
import Breadcrumb from './components/Sidebar/Breadcrumb';
import Sidebar from './components/Sidebar/Sidebar'
import FruitPage from './components/Fruit-Page/Fruit'
import IogurtPage from './components/Yogurt-Page/Yogurt'
import IceCreamPage from './components/IceCream-Page/IceCream'
import SnackPage from './components/Snack-Page/Snack'
import kenzie from './assets/kenzie-footer.png'
import {
  Switch,
  Route
} 
from "react-router-dom";
import { withRouter } from 'react-router';

const { Header, Content } = Layout;

class App extends React.Component {

  render() {
    return (
      <Layout>
        <Header className="header">
          <div style={{ color: "white", fontWeight: "bold" }}> 
            <img src={ kenzie } alt="" width={50} /> Cantina Kenzie 
          </div> 
        </Header>

        <Layout>
          <Sidebar pathname={this.props.location.pathname}/>

          <Layout style={{ padding: '0 24px 24px' }}>

          <Breadcrumb style={{ margin: '16px 0' }} />

            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
              }}>
              
              <Switch>
                <Route path="*/Frutas" key="1">
                  <FruitPage />
                </Route>
                <Route path="*/Iogurtes" key="2">
                  <IogurtPage />
                </Route>
                <Route path="*/Sorvetes" key="3">
                  <IceCreamPage />
                </Route>
                <Route path="*/Salgadinhos" key="4">
                  <SnackPage />
                </Route>
                <Route path="*/" key="1">
                  <Home />
                </Route>
              </Switch>
            
            </Content>            
          </Layout>
        </Layout>
      </Layout>
    )
  }
}

export default withRouter(App);

function Home() {
return <FruitPage />
}
