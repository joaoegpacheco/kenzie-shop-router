import React from 'react'
import { Row } from 'antd'

import ProductButton from '../Product-Button/ProductButton'

const FruitPage = (props) => {
  //Para renderizar vários produtos, crie uma variável products que executa um map em cima de um array de dados recebido por prop
  //E que retorna um array de <ProductButton/> 
  //Passando as devidas props para o <ProductButton/>

  //const products = props. ...

  //Coloquei apenas um <ProductButton/> dentro do Row para exemplificar
  //Remova-o antes de entregar a avaliação
  //Use só a variável products

  const fruits = [{ title:"Banana",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h3c/h4c/h00/h00/14506624385054.jpg",
                  description:"R$4,99"
                },
                { title:"Morango",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h54/h99/h00/h00/12347524087838.jpg",
                  description:"R$5,99"
                },
                { title:"Laranja",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hcd/he2/h00/h00/14088525545502.jpg",
                  description:"R$5,99"
                },
                { title:"Maça",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/ha4/h6e/h00/h00/13957619187742.jpg",
                  description:"R$3,99"
                }              
              ];

  const products = fruits.map((item) => <ProductButton title={item.title} src={item.src} description={item.description}/>)

  return (
    <Row justify="space-between" gutter={24}>
      {products}
      {/* <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h3c/h4c/h00/h00/14506624385054.jpg" title="Banana" description="R$ 4,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h54/h99/h00/h00/12347524087838.jpg" title="Morango" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hcd/he2/h00/h00/14088525545502.jpg"title="Laranja" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/ha4/h6e/h00/h00/13957619187742.jpg" title="Maça" description="R$ 3,99"/> */}
    </Row>
  )
}
export default FruitPage