import React from 'react'
import { Row } from 'antd'

import ProductButton from '../Product-Button/ProductButton'

const IceCreamPage = (props) => {
  //Para renderizar vários produtos, crie uma variável products que executa um map em cima de um array de dados recebido por prop
  //E que retorna um array de <ProductButton/> 
  //Passando as devidas props para o <ProductButton/>

  //const products = props. ...

  //Coloquei apenas um <ProductButton/> dentro do Row para exemplificar
  //Remova-o antes de entregar a avaliação
  //Use só a variável products

  const icecreams = [{ title:"Cookies e Creme",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h2a/h87/h00/h00/11457145929758.jpg",
                  description:"R$4,99"
                },
                { title:"Baunilha com Macadâmia",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/ha7/h83/h00/h00/13740803096606.jpg",
                  description:"R$5,99"
                },
                { title:"Doce de Leite",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h70/h33/h00/h00/13740798640158.jpg",
                  description:"R$5,99"
                },
                { title:"Morango",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hac/h46/h00/h00/13740803489822.jpg",
                  description:"R$3,99"
                }              
              ];

  const products = icecreams.map((item) => <ProductButton title={item.title} src={item.src} description={item.description}/>)

  return (
    <Row justify="space-between" gutter={24}>
      {products}
      {/* <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h2a/h87/h00/h00/11457145929758.jpg" title="Cookies e Creme" description="R$ 4,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/ha7/h83/h00/h00/13740803096606.jpg" title="Baunilha com Macadâmia" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h70/h33/h00/h00/13740798640158.jpg"title="Doce de Leite" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hac/h46/h00/h00/13740803489822.jpg" title="Morango" description="R$ 3,99"/> */}
    </Row>
  )
}
export default IceCreamPage