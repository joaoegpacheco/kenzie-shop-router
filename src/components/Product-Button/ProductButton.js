import React from 'react'
import { Card } from 'antd'
import { Col } from 'antd'

const { Meta } = Card

const ProductButton = (props) => {
  return (
      <Col span={6}>
        <Card
          hoverable
          cover={<img alt={props.title} src={props.src}></img>}
        >
          <Meta title={props.title} description={props.description}></Meta>
        </Card>
      </Col>
  )
}

export default ProductButton