import React from 'react';
import { useLocation } from 'react-router-dom';
import { Breadcrumb } from 'antd';
const { Item } = Breadcrumb;

const BreadCrumbs = () => {
  let location = useLocation();
  let setPage = () => {
    if (location.pathname === '/') {
      return 'Frutas';
    }
    if (location.pathname === '/frutas') {
      return 'Frutas';
    }
    if (location.pathname === '/iogurtes') {
      return 'Iogurtes';
    }
    if (location.pathname === '/sorvetes') {
      return 'Sorvetes';
    }
    if (location.pathname === '/salgadinhos') {
      return 'Salgadinhos';
    }
  };

  return (
    <Breadcrumb>
      <Item>Loja</Item>
      <Item>Categorias</Item>
      <Item>{setPage()}</Item>
    </Breadcrumb>
  );
};

export default BreadCrumbs;