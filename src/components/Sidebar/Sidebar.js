import React from 'react'
import { Layout, Menu } from 'antd';
import { GiftOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";

const { Sider } = Layout;
const { SubMenu, Item } = Menu;

const Sidebar = (props) => {

  const setPage = () => {
    if (props.pathname === '/iogurtes') {
      return "2"
    }
    else if (props.pathname === '/sorvetes') {
      return "3"
    }
    else if (props.pathname === '/salgadinhos') {
      return "4"
    }
    return "1"
  }

  return (
    <Sider width={200} className="site-layout-background" style={{ height: "100vh", position: "sticky", top: 0 }}>
      <Menu
        mode="inline"
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        style={{ height: '100%', borderRight: 0 }}
        selectedKeys={[setPage()]}
      >
        <SubMenu key="sub1" icon={<GiftOutlined />} title="Categorias">
          <Item key="1"><Link to="/frutas" />Frutas</Item>
          <Item key="2"><Link to="/iogurtes" />Iogurtes</Item>
          <Item key="3"><Link to="/sorvetes" />Sorvetes</Item>
          <Item key="4"><Link to="/salgadinhos" />Salgadinhos</Item>
        </SubMenu>
      </Menu>
    </Sider>
  )
}  

export default Sidebar