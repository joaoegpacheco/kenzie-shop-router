import React from 'react'
import { Row } from 'antd'

import ProductButton from '../Product-Button/ProductButton'

const SnackPage = (props) => {
  //Para renderizar vários produtos, crie uma variável products que executa um map em cima de um array de dados recebido por prop
  //E que retorna um array de <ProductButton/> 
  //Passando as devidas props para o <ProductButton/>

  //const products = props. ...

  //Coloquei apenas um <ProductButton/> dentro do Row para exemplificar
  //Remova-o antes de entregar a avaliação
  //Use só a variável products

  const snacks = [{ title:"Doritos Sabor Queijo Nacho",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h65/h6f/h00/h00/13389309050910.jpg",
                  description:"R$4,99"
                },
                { title:"Fandangos Sabor Presunto",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h84/h73/h00/h00/12209254694942.jpg",
                  description:"R$5,99"
                },
                { title:"Cheetos Tubo Sabor Cheddar",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h51/h98/h00/h00/13024222052382.jpg",
                  description:"R$5,99"
                },
                { title:"Cebolitos",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hc2/h3a/h00/h00/9455801729054.jpg",
                  description:"R$3,99"
                }              
              ];

  const products = snacks.map((item) => <ProductButton title={item.title} src={item.src} description={item.description}/>)

  return (
    <Row justify="space-between" gutter={24}>
      {products}
      {/* <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h65/h6f/h00/h00/13389309050910.jpg" title="Doritos Sabor Queijo Nacho" description="R$ 4,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h84/h73/h00/h00/12209254694942.jpg" title="Fandangos Sabor Presunto" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h51/h98/h00/h00/13024222052382.jpg"title="Cheetos Tubo Sabor Cheddar" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hc2/h3a/h00/h00/9455801729054.jpg" title="Cebolitos" description="R$ 3,99"/> */}
    </Row>
  )
}
export default SnackPage