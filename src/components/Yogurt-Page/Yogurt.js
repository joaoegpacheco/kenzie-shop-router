import React from 'react'
import { Row } from 'antd'

import ProductButton from '../Product-Button/ProductButton'

const YogurtPage = (props) => {
  //Para renderizar vários produtos, crie uma variável products que executa um map em cima de um array de dados recebido por prop
  //E que retorna um array de <ProductButton/> 
  //Passando as devidas props para o <ProductButton/>

  //const products = props. ...

  //Coloquei apenas um <ProductButton/> dentro do Row para exemplificar
  //Remova-o antes de entregar a avaliação
  //Use só a variável products

  const yogurts = [{ title:"Natural",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hd0/h5b/h00/h00/12205623705630.jpg",
                  description:"R$4,99"
                },
                { title:"Baunilha",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/h09/h9e/h00/h00/12205624360990.jpg",
                  description:"R$5,99"
                },
                { title:"Frutas Silvestres",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hd6/h4b/h00/h00/12205625999390.jpg",
                  description:"R$5,99"
                },
                { title:"Mel",
                  src:"https://static.carrefour.com.br/medias/sys_master/images/images/hf2/h52/h00/h00/12205625344030.jpg",
                  description:"R$3,99"
                }              
              ];

  const products = yogurts.map((item) => <ProductButton title={item.title} src={item.src} description={item.description}/>)

  return (
    <Row justify="space-between" gutter={24}>
      {products}
      {/* <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hd0/h5b/h00/h00/12205623705630.jpg" title="Natural" description="R$ 4,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/h09/h9e/h00/h00/12205624360990.jpg" title="Baunilha" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hd6/h4b/h00/h00/12205625999390.jpg"title="Frutas Silvestres" description="R$ 5,99"/>
      <ProductButton src="https://static.carrefour.com.br/medias/sys_master/images/images/hf2/h52/h00/h00/12205625344030.jpg" title="Mel" description="R$ 3,99"/> */}
    </Row>
  )
}
export default YogurtPage